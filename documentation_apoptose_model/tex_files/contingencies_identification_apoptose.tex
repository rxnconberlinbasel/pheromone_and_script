% ------------------------------------------------------------------------
% bjourdoc.tex for birkjour.cls*******************************************
% ------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{birkjour}



\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}[thm]{Corollary}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\theoremstyle{remark}
\newtheorem{rem}[thm]{Remark}
\newtheorem*{ex}{Example}
\numberwithin{equation}{section}
\renewcommand{\familydefault}{\sfdefault}
\usepackage{hyperref}
\setlength{\parindent}{2mm}
\setlength{\parskip}{2mm}
\usepackage{setspace}
\onehalfspacing
\parindent0pt
\usepackage[utf8]{inputenc}


%\geometry{a4paper,left=3.5cm, right=3cm, top=3cm, bottom=3cm}
\begin{document}


\title[]
 {Proof of concept: \\Network based contingencies \mbox{identification} \\on the example of the apopotose pathway}

{\href{mailto:j.linnik@fu-berlin.de}{Jana Linnik: j.linnik@fu-berlin.de}\\
05/05/2014}


%\begin{abstract}
%\end{abstract}
\vspace{0.5cm}
%%% ----------------------------------------------------------------------
\maketitle
%%% ----------------------------------------------------------------------
\vspace{1.5cm}
\tableofcontents
\newpage
\section{Introduction and Summary}
The purpose of this document is to explain how the contingencies identification script can be used to determine contingencies. The script uses a simple true/false query and requires reaction network information on biological complexes and reaction edges in a special format. \\
The script works only for reactions producing elemental states that can serve as modifiers for contingencies. The treatment of reactions that consume elemental states as dephosphorylation or even destroy species as degradation is not implemented. Such reactions have to be treated separately with a differently defined query.\\
A disadvantage of this true/false query is that it is restricted to species which are involved in not more then four reactions.\\
The rxncon apoptose model in \textit{M. Rother et al., Mol. BioSyst.} \textbf{9} (2013), 1993 \cite{Rother2013} serves as an \mbox{example} model. Using the appropriate input format, all 28 contingencies involving 9 species were identified (see Fig. 1).

\section{Procedure}
\subsection{Create a reaction list}
First, all reactions of the model has to be identified. For the apoptose model a reaction list already exists and I will use these reactions (see page 2002 in \cite{Rother2013}).
\subsection{Identify overlapping reactions}
Secondly, one have to identify the so called 'overlapping' reactions. Reactions overlap, if they have one species in common. As an overview, I create a species list, where all their reactions are summarized (shown in Table 1). However, species that are involved in only one reaction do not have to be considered.

\begin{table}[htbp]
\label{specieslist}
\centering
\caption{Species list. All species and their reactions are listed. Reactions that have one species in common can be considered as 'overlapping'. Species, that participate in only one reaction are omitted.}
\begin{tabular}{c c|c}
& \textbf{species}&\textbf{reactions}  \vspace{0.2cm} \\ 
1) & R & TRAIL i R\\
  & & FADD ppi R\\
  &  & FLIP ppi R\\
  &   & R CUT C8\vspace{0.2cm}\\

2) & C8 & R CUT C8\\
  & & C6 CUT C8\\
  &  & C8 CUT C3\\
  &   & C8 ppi BAR\\  
    &   & C8 CUT BID\vspace{0.2cm}\\  
    
    3) & C6 & C6 CUT C8\\
  & & C3 CUT C6\vspace{0.2cm}\\  

4) & C3 & C3 CUT C6\\
  & & C3 CUT ECS\\
  &  & C3 ppi XIAP\\
    &   & C9 CUT C3\vspace{0.2cm}\\  
    
5) & BID & C8 CUT BID\\
  & & BID ppi MCL1\\
  &  & BID Activate BAX\vspace{0.2cm}\\  
 
6) & C9 & C9 ppi APAF\\
  & & C9 ppi XIAP\\
  &  & C9 CUT C3 \vspace{0.2cm}\\  
  
7) & XIAP & C3 ppi XIAP\\
  & & C9 ppi XIAP\\
  &  & SMAC ppi XIAP\vspace{0.2cm}\\  
  
8) & BAX & BID Activate BAX\\
  & & MT MIMP BAX\\
  &  & BCL2 ppi BAX\\
  &   & BAX MEXP Cytoc \\  
    &   & BAX MEXP SMAC \\  
      &   & BAX ppi BAX \\  
    &   & BAX tet BAX\vspace{0.2cm}\\  
    
9) & Cytoc & BAX MEXP Cytoc\\
  & & Cytoc ppi APAF\vspace{0.2cm}\\  
  
10) & SMAC & BAX MEXP SMAC\\
  & & SMAC ppi XIAP\vspace{0.2cm}\\  

11) & APAF & Cytoc ppi APAF\\
  & & C9 ppi APAF\vspace{0.2cm}\\  

\end{tabular}
\end{table}

\subsection{Identify existing states and reaction edges for each species}
Now, one have to identify all biological complexes and the reaction edges between them. This information can be derived from the network. In a SBGN-PD graph, each biological complex node and each process node connected to a complex trough a modification or catalysis edge, corresponds to one state (i.e. an elemental state or a conjunction of elemental states). Reaction edges can be just read off, whereby production, catalysis, consumption and modification arcs can correspond to a reaction edge.\par
Further, I will use the following abbreviations: Reactions are capital letters (A, B, C, ...), elemental states are lower case letters (a, b, c, ...), respectively, and states, that correspond to a conjunction of elemental states are strings of lower case letters (ab, ac, abc, ...). Reaction edges are abbreviated as follows: ab/a, abc/ab etc., whereby the first state is the product state and the second one the source state. \par
As already mentioned, the current script for contingencies identification works only for species with four or less overlapping'reactions. I will consider only species which fulfill this requirement, hence the species 1),\, 3),\, 4),\, 5),\, 6),\, 7),\, 9)\, 10) and 11) (compare Table 1).

Let us consider how the input for species R has to be defined.\\
First, I give each reaction an abbreviation as summarized in Table 2. 



\begin{table}[hbtp]
\label{abbrevR}
\centering
\caption{Abbreviations for reactions of species R.}
\begin{tabular}{c | c c | c }
 & reaction state & elemental state & \vspace{0.5em} \\
TRAIL i R & A & a & TRAIL--R\\
FADD ppi R & B & b & FADD--R\\
FLIP ppi R & C & c & FLIP--R\\
R CUT C8  &  D & d & C8--$\lbrace$Truncated$\rbrace$\\
\end{tabular}
\end{table}

Now, I create a list that contain all existing states of species R and a list that contain all existing reaction edges, whereby I use the state notation introduced above in Table 2. \par
\texttt{R\textunderscore biological\textunderscore states = (a, ab, abc, abd)} \par
\texttt{R\textunderscore reaction\textunderscore edges = (ab/a, abc/ab, abd/ab)} \par
Here, reaction R3 in the SBGN-PD graph corresponds to '\texttt{ab/a}', reaction R7 to '\texttt{abc/ab}' and reaction R6 to '\texttt{abd/ab}'. The dissociations are neglected. For instance, the state '\texttt{abd}' represents that C8 is truncated by R, when R is bound to TRAIL and to FADD, hence three elemental states of R are conjoint. \\
Currently, contingencies have to be defined differently for reversible and irreversible reactions, that's why we have to assign the reactions. We can just define an additional list:\par
\texttt{R\textunderscore reversible\textunderscore reactions = (A, B, C)}\par
The script needs also the information in how many reactions the considered species is involved. For this purpose we list all reactions: \\
\texttt{R\textunderscore existing\textunderscore reactions = (A, B, C, D)}\par
Finally, this is the required information. Similar lists for the other species are shown below.

\begin{table}[hbtp]
\label{abbrevC6}
\centering
\caption{Abbrev. and input for reactions of species C6.}
\begin{tabular}{c | c c | c }
 & reaction state & elemental state & \vspace{0.5em} \\
C6 CUT C8 & A & a & C8--$\lbrace$Truncated$\rbrace$ \\
C3 CUT C6 & B & b & C6--$\lbrace$Truncated$\rbrace$\vspace{0.2cm}\\ 
\multicolumn{3}{l}{\texttt{C8\textunderscore biological\textunderscore states = (b, ab)}} \\
\multicolumn{3}{l}{\texttt{C8\textunderscore reaction\textunderscore edges = (ab/b)}}\\
\multicolumn{3}{l}{\texttt{C8\textunderscore existing\textunderscore reactions = (A, B)}}\\
\multicolumn{3}{l}{\texttt{C8\textunderscore reversible\textunderscore reactions = ()}}\\

\end{tabular}
\end{table}
\begin{table}[hbtp]
\label{abbrevC3}
\centering
\caption{Abbrev. for reactions of species C3.}
\begin{tabular}{c | c c | c }
 & reaction state & elemental state & \vspace{0.5em} \\
C3 CUT C6 & A & a & C6--$\lbrace$Truncated$\rbrace$\\
C3 CUT ECS & B & b & ECS--$\lbrace$Truncated$\rbrace$\\
C3 ppi XIAP & C & c & C3--XIAP\\
C9 CUT C3 & D & d & C3--$\lbrace$Truncated$\rbrace$\vspace{0.2cm}\\ 
\multicolumn{3}{l}{\texttt{C3\textunderscore biological\textunderscore states = (d, ad, bd, cd)}} \\
\multicolumn{3}{l}{\texttt{C3\textunderscore reaction\textunderscore edges = (ad/d, bd/d, cd/d)}}\\
\multicolumn{3}{l}{\texttt{C3\textunderscore existing\textunderscore reactions = (A, B, C, D)}}\\
\multicolumn{3}{l}{\texttt{C3\textunderscore reversible\textunderscore reactions = (C)}}\\  
\end{tabular}
\end{table}

\begin{table}[hbtp]
\label{abbrevBID}
\centering
\caption{Abbrev. for reactions of species BID.}
\begin{tabular}{c | c c | c }
 & reaction state & elemental state & \vspace{0.5em} \\
C8 CUT BID & A & a & BID--$\lbrace$Truncated$\rbrace$\\
BID ppi MCL1 & B & b & BID--MCL1\\
BID Activate BAX & C & c & BAX--$\lbrace$active$\rbrace$\vspace{0.2cm}\\ 
\multicolumn{3}{l}{\texttt{BID\textunderscore biological\textunderscore states = (a, ab, ac)}} \\
\multicolumn{3}{l}{\texttt{BID\textunderscore reaction\textunderscore edges = (ab/a, ac/a)}}\\
\multicolumn{3}{l}{\texttt{BID\textunderscore existing\textunderscore reactions = (A, B, C)}}\\
\multicolumn{3}{l}{\texttt{BID\textunderscore reversible\textunderscore reactions = (B)}}\\  
\end{tabular}
\end{table}

\begin{table}[hbtp]
\label{abbrevC9}
\centering
\caption{Abbrev. for reactions of species C9.}
\begin{tabular}{c | c c | c }
 & reaction state & elemental state & \vspace{0.5em} \\
C9 ppi APAF& A & a & C9--APAF\\
C9 ppi XIAP& B & b & C9--XIAP\\
C9 CUT C3& C & c & C3--$\lbrace$Truncated$\rbrace$\vspace{0.2cm}\\ 
\multicolumn{3}{l}{\texttt{C9\textunderscore biological\textunderscore states = (a, ab, ac)}} \\
\multicolumn{3}{l}{\texttt{C9\textunderscore reaction\textunderscore edges = (ab/a, ac/a)}}\\
\multicolumn{3}{l}{\texttt{C9\textunderscore existing\textunderscore reactions = (A, B, C)}}\\
\multicolumn{3}{l}{\texttt{C9\textunderscore reversible\textunderscore reactions = (A, B)}}\\
\end{tabular}
\end{table}

\begin{table}[hbtp]
\label{abbrevXIAP}
\centering
\caption{Abbrev. for reactions of species XIAP.}
\begin{tabular}{c | c c | c }
 & reaction state & elemental state & \vspace{0.5em} \\
C3 ppi XIAP & A & a & C3--XIAP\\
C9 ppi XIAP & B & b & C9--XIAP\\
SMAC ppi XIAP & C & c & SMAC--XIAP\vspace{0.2cm}\\ 
\multicolumn{3}{l}{\texttt{XIAP\textunderscore biological\textunderscore states = (a, b, c)}} \\
\multicolumn{3}{l}{\texttt{XIAP\textunderscore reaction\textunderscore edges = ()}}\\
\multicolumn{3}{l}{\texttt{XIAP\textunderscore existing\textunderscore reactions = (A, B, C)}}\\
\multicolumn{3}{l}{\texttt{XIAP\textunderscore reversible\textunderscore reactions = (A, B, C)}}\\
\end{tabular}
\end{table}
 
\begin{table}[hbtp]
\label{abbrevCytoc}
\centering
\caption{Abbrev. for reactions of species Cytoc.}
\begin{tabular}{c | c c | c }
 & reaction state & elemental state & \vspace{0.5em} \\
BAX MEXP Cytoc & A & a & Cytoc--$\lbrace$Cytoplasm$\rbrace$\\
Cytoc ppi APAF & B & b & Cytoc--APAF\vspace{0.2cm}\\ 
\multicolumn{3}{l}{\texttt{Cytoc\textunderscore biological\textunderscore states = (a, ab)}} \\
\multicolumn{3}{l}{\texttt{Cytoc\textunderscore reaction\textunderscore edges = (ab/a)}}\\
\multicolumn{3}{l}{\texttt{Cytoc\textunderscore existing\textunderscore reactions = (A, B)}}\\
\multicolumn{3}{l}{\texttt{Cytoc\textunderscore reversible\textunderscore reactions = (B)}}\\
\end{tabular}
\end{table}

\begin{table}[hbtp]
\label{abbrevSMAC}
\centering
\caption{Abbrev. for reactions of species SMAC.}
\begin{tabular}{c | c c | c }
 & reaction state & elemental state & \vspace{0.5em} \\
BAX MEXP SMAC & A & a & SMAC--$\lbrace$Cytoplasm$\rbrace$\\
SMAC ppi XIAP & B & b & SMAC--XIAP\vspace{0.2cm}\\ 
\multicolumn{3}{l}{\texttt{SMAC\textunderscore biological\textunderscore states = (a, ab)}} \\
\multicolumn{3}{l}{\texttt{SMAC\textunderscore reaction\textunderscore edges = (ab/a)}}\\
\multicolumn{3}{l}{\texttt{SMAC\textunderscore existing\textunderscore reactions = (A, B)}}\\
\multicolumn{3}{l}{\texttt{SMAC\textunderscore reversible\textunderscore reactions = (B)}}\\
\end{tabular}
\end{table}

\begin{table}[hbtp]
\label{abbrevAPAF}
\centering
\caption{Abbrev. for reactions of species APAF.}
\begin{tabular}{c | c c | c }
 & reaction state & elemental state & \vspace{0.5em} \\
Cytoc ppi APAF & A & a & Cytoc--APAF\\
C9 ppi APAF & B & b & C9--APAF\vspace{0.2cm}\\ 
\multicolumn{3}{l}{\texttt{APAF\textunderscore biological\textunderscore states = (a, ab)}} \\
\multicolumn{3}{l}{\texttt{APAF\textunderscore reaction\textunderscore edges = (ab/a)}}\\
\multicolumn{3}{l}{\texttt{APAF\textunderscore existing\textunderscore reactions = (A, B)}}\\
\multicolumn{3}{l}{\texttt{APAF\textunderscore reversible\textunderscore reactions = (A, B)}}\\
\end{tabular}
\end{table}
  
\subsection{Semi-Automatic Contingencies Identification}
Using the lists above, all necessary contingencies can be identified for each species at once. The input lists has to be written in an input file, e.g. \texttt{input\textunderscore species.py} and the script can be executed in a shell as following: \\
\texttt{python contingencies\textunderscore identification.py input\textunderscore species} \par

The output for each species is written out in a separate text file (can be specified in the input file). The output has the following raw format:\\
\texttt{"Reaction"; "contingency symbol" "elemental state"} \\
while the respective abbreviations for reactions and elemental states are used, i.\,e. \texttt{A;\,!\,b.} Hence, one has to re-translate the abbreviations. 

\subsection{Final Model Creation}
Finally, one can merge all the re-translated contingencies text files that yields to the complete model. In our apoptose model examplem, however, we are missing the contingencies for which the elemental states of species BAX and C3 are modifiers, because these species are involved in more then four reactions.

\begin{figure}[htbp]\label{complete_contingencies}
\caption{Merged and re-translated contingencies for the apoptose model: All contingencies for which the considered species serve as modifiers, i.e. species 1), 3) - 7) and 9) - 11). Missing species are BAX and C3.} \vspace{1em}

\begin{flushleft}

\hspace{4em}\texttt{BID\textunderscore ppi\textunderscore MCL1; ! BID-$\lbrace$Truncated$\rbrace$} \\
\hspace{4em}\texttt{BID\textunderscore Activate\textunderscore BAX; ! BID-$\lbrace$Truncated$\rbrace$} \\
\hspace{4em}\texttt{BID\textunderscore Activate\textunderscore BAX; x BID--MCL1} \\
\hspace{4em}\texttt{C3\textunderscore CUT\textunderscore C6; x C3--XIAP } \\
\hspace{4em}\texttt{C3\textunderscore CUT\textunderscore C6; ! C3-$\lbrace$Truncated$\rbrace$} \\
\hspace{4em}\texttt{C3\textunderscore CUT\textunderscore ECS; x C3--XIAP } \\
\hspace{4em}\texttt{C3\textunderscore CUT\textunderscore ECS; ! C3-$\lbrace$Truncated$\rbrace$} \\
\hspace{4em}\texttt{C3\textunderscore ppi\textunderscore XIAP; ! C3-$\lbrace$Truncated$\rbrace$} \\
\hspace{4em}\texttt{C3\textunderscore ppi\textunderscore XIAP; x C9--XIAP} \\
\hspace{4em}\texttt{C3\textunderscore ppi\textunderscore XIAP; x SMAC--XIAP} \\
\hspace{4em}\texttt{C9\textunderscore ppi\textunderscore APAF; x C9--XIAP} \\
\hspace{4em}\texttt{C9\textunderscore ppi\textunderscore APAF; ! Cytoc--APAF} \\
\hspace{4em}\texttt{C9\textunderscore ppi\textunderscore XIAP; ! C9--APAF} \\
\hspace{4em}\texttt{C9\textunderscore ppi\textunderscore XIAP; x C3--XIAP} \\
\hspace{4em}\texttt{C9\textunderscore ppi\textunderscore XIAP; x SMAC--XIAP} \\
\hspace{4em}\texttt{C9\textunderscore CUT\textunderscore C3; ! C9--APAF} \\
\hspace{4em}\texttt{C9\textunderscore CUT\textunderscore C3; x C9--XIAP} \\
\hspace{4em}\texttt{Cytoc\textunderscore ppi\textunderscore APAF ; ! Cytoc-$\lbrace$Cytoplasm$\rbrace$} \\
\hspace{4em}\texttt{Cytoc\textunderscore ppi\textunderscore APAF ; x C9--APAF} \\
\hspace{4em}\texttt{TRAIL\textunderscore i\textunderscore R; x FADD--R } \\
\hspace{4em}\texttt{FADD\textunderscore ppi\textunderscore R; ! TRAIL--R} \\
\hspace{4em}\texttt{FADD\textunderscore ppi\textunderscore R; x FLIP--R } \\
\hspace{4em}\texttt{FLIPP\textunderscore ppi\textunderscore R; ! FADD--R } \\
\hspace{4em}\texttt{R\textunderscore CUT\textunderscore C8; ! FADD--R } \\
\hspace{4em}\texttt{R\textunderscore CUT\textunderscore C8; x FLIP--R } \\
\hspace{4em}\texttt{SMAC\textunderscore ppi\textunderscore XIAP ; ! SMAC-$\lbrace$Cytoplasm$\rbrace$} \\
\hspace{4em}\texttt{SMAC\textunderscore ppi\textunderscore XIAP; x C3--XIAP} \\
\hspace{4em}\texttt{SMAC\textunderscore ppi\textunderscore XIAP; x C9--XIAP } \\
\end{flushleft}

\end{figure}

\section{Outlook}
One objective is to import biochemical reaction network information from SBML models and to translate them in reaction contingency models. Challenging issues are the read-out of the required information and the scalability, i.e. how to define a contingencies identification in a manner that is independent of the number of overlapping reactions.

\begin{thebibliography}{1}
\bibitem{Rother2013} Rother M., Münzner U., Thieme S., Krantz M., {Information content and scalability in signal
transduction network reconstruction formats}, \textit{Mol. BioSyst.}
$\boldsymbol{9}$ (2013), 1993.

\end{thebibliography}

% ------------------------------------------------------------------------
\end{document}
% ------------------------------------------------------------------------
