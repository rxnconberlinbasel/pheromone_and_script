#!/usr/bin/env python

"""
Apoptose model
Input for Species C3
"""

existing_reactions = ['A', 'B', 'C', 'D'] # All overlapping reactions for this species

reversible_reactions = ['C']

biological_states = ['d', 'ad', 'bd', 'cd']   # Complexes: Conjunction of elemental states
reaction_edges = ['ad/d', 'bd/d', 'cd/d']   # Direction of the ractions: ab/a means a -> ab

"""File in which contingencies for this species"""
all_contingencies = open('output_files/raw_contingencies_C3', 'w+')