#!/usr/bin/env python

"""
Apoptose model
Input for Species R
"""

existing_reactions = ['A', 'B', 'C', 'D'] # All overlapping reactions for this species

reversible_reactions = ['A', 'B', 'C']

biological_states = ['a', 'ab', 'abc', 'abd']   # Complexes: Conjunction of elemental states
reaction_edges = ['ab/a', 'abc/ab', 'abd/ab']   # Direction of the ractions: ab/a means a -> ab

"""File in which contingencies for this species"""
all_contingencies = open('output_files/raw_contingencies_R', 'w+')