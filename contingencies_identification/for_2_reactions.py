#!/usr/bin/env python
"""
For two existing reactions:
"""

def find_contingencies_2reactions(input_considered_species):
    """
    """

    """Input import:"""
    exec("from " + input_considered_species + " import *")

    """Variables import:"""
    from contingencies_identification_alpha import *
    
    """Modules import:"""
    from no_reaction_state import if_no_reaction_state
    from yes_reaction_state import if_reaction_state_exists


    if intersection_first not in biological_states and \
        'abc' not in biological_states and \
        conting_state_reaction in reversible_reactions:
        print >> all_contingencies,'%s ; x %s (1) (%s is absolute inhibitory for %s)' \
        % (cons_reaction, conting_state, conting_state, cons_reaction)

    elif reaction_state not in biological_states:
        if_no_reaction_state(input_considered_species)
        
    
    elif reaction_state in biological_states:
        if_reaction_state_exists(input_considered_species)