#!/usr/bin/env python

"""
Apoptose model
Input for Species XIAP
"""

existing_reactions = ['A', 'B', 'C'] # All overlapping reactions for this species

reversible_reactions = ['A', 'B', 'C']

biological_states = ['a', 'b', 'c']   # Complexes: Conjunction of elemental states
reaction_edges = []   # Reaction direction: ab/a means a -> ab

"""File in which contingencies for this species"""
all_contingencies = open('output_files/raw_contingencies_XIAP', 'w+')