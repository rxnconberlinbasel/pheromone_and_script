#!/usr/bin/env python

"""
Apoptose model
Input for Species C6
"""

existing_reactions = ['A', 'B'] # All overlapping reactions for this species

reversible_reactions = []

biological_states = ['b', 'ab']   # Complexes: Conjunction of elemental states
reaction_edges = ['ab/b']   # Reaction direction: ab/a means a -> ab

"""File in which contingencies for this species"""
all_contingencies = open('output_files/raw_contingencies_C6', 'w+')