#!/usr/bin/env python

"""
Apoptose model
Input for Species SMAC
"""

existing_reactions = ['A', 'B'] # All overlapping reactions for this species

reversible_reactions = ['B']

biological_states = ['a', 'ab']   # Complexes: Conjunction of elemental states
reaction_edges = ['ab/a']   # Reaction direction: ab/a means a -> ab

"""File in which contingencies for this species"""
all_contingencies = open('output_files/raw_contingencies_SMAC', 'w+')