#!/usr/bin/env python
"""
If reaction state exsists in biological states, 
i. e. if the elemental state of the considered 
reaction also occur as a biological state in 
the model.
"""

def if_reaction_state_exists(input_considered_species):
    """
    """

    """Input import:"""
    exec("from " + input_considered_species + " import *")

    """Variables import:"""
    from contingencies_identification_alpha import *

    """Modules import:"""
    from conting_state_reversible import conting_state_rev
   # from consid_reaction_reversible import consid_reaction_rev


    if conting_state in biological_states and \
        intersection_first +'/'+ conting_state not in reaction_edges and \
        conting_state not in degradatation_states:
        print >> all_contingencies, '%s ; x %s (1) (%s exists, but no edge from %s/%s)' \
        % (cons_reaction, conting_state, conting_state, intersection_first, conting_state)
    
    ######### for reversible states in old definition ######
    elif conting_state_reaction in reversible_reactions:
        conting_state_rev(input_considered_species)
    ########################################################
    
    elif intersection_first+'/'+conting_state in reaction_edges or \
        intersection_second_1+'/'+aux_state_1 in reaction_edges or \
        intersection_second_2+'/'+aux_state_2 in reaction_edges or \
        'abcd/'+all_other in reaction_edges:
        print >> all_contingencies, '''no contingency (1) for %s and %s, %s don't block %s in general''' \
        % (cons_reaction, conting_state, conting_state, cons_reaction)  
          
    else:
        print >> all_contingencies, 'no contingency (2) for %s and %s' \
        % (cons_reaction, conting_state) 