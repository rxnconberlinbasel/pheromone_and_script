#!/usr/bin/env python
"""
If reaction state not in biological states, 
i. e. if the elemental state of the considered 
reaction doesn't exist as biological state
in the model.

"""

def if_no_reaction_state(input_considered_species):
    """
    """

    """Input import:"""
    exec("from " + input_considered_species + " import *")
    """Variables import:"""
    from contingencies_identification_alpha import *

    """Modules import:"""
    from conting_state_reversible import conting_state_rev


    if intersection_first in biological_states:
        print >> all_contingencies, '%s ; ! %s (1) (1st intersection state %s is produced)' \
        % (cons_reaction, conting_state, intersection_first)
    
    elif intersection_second_1 + '/' + aux_state_1 in reaction_edges and \
        aux_for_mark_1a not in biological_states and \
        conting_state not in biological_states:
        print >> all_contingencies, '%s ; ! %s (2) (2nd intersection state %s is produced)' \
        % (cons_reaction, conting_state, intersection_second_1)
    
    elif intersection_second_2 +'/'+ aux_state_2 in reaction_edges and \
        aux_for_mark_1b not in biological_states:
        print >> all_contingencies, '%s ; ! %s (2) (2nd intersection state %s is produced)' \
        % (cons_reaction, conting_state, intersection_second_2)
    
    elif 'abcd/'+ all_other in reaction_edges and \
        aux_state_1 not in biological_states and \
        intersection_second_1 not in biological_states and \
        intersection_second_2 not in biological_states and \
        aux_state_2 not in biological_states: 
        print >> all_contingencies, '%s ; ! %s (3) (3rd intersection state abcd is produced)' \
        % (cons_reaction, conting_state)
                       

    ###### for reversible states in old definition #################################
    elif conting_state_reaction in reversible_reactions:
        conting_state_rev(input_considered_species)
    #################################################################################

    elif reaction_state not in biological_states and \
        intersection_first+'/'+conting_state in reaction_edges or \
        intersection_second_1+'/'+aux_state_1 in reaction_edges or \
        intersection_second_2+'/'+aux_state_2 in reaction_edges or \
        'abcd/'+all_other in reaction_edges:
        print >> all_contingencies, \
            '''no contingency (1) for %s and %s, %s don't block %s in general''' \
            % (cons_reaction, conting_state, conting_state, cons_reaction)  
          
