#!/usr/bin/env python
"""
If contingency state is produced by a reversibel reaction:

Check wether the contingency is 'x' to avoid undesirable complexes. 

(Old definition for reversible reactions)
"""

def conting_state_rev(input_considered_species):
    """"""

    """Input import:"""
    exec("from " + input_considered_species + " import *")

    """Variables import:"""
    from contingencies_identification_alpha import *

    if conting_state in biological_states and \
        intersection_first not in biological_states and \
        aux_for_mark_1a not in biological_states and \
        aux_for_mark_1b not in biological_states and \
        aux_for_mark_2 not in biological_states and \
        reaction_state not in biological_states:
        pass

    # """If first order intersection exists:"""
    elif intersection_first +'/'+ reaction_state in reaction_edges and \
        intersection_first +'/'+ conting_state not in reaction_edges:

        """
        Check wether the reaction producing the contingency state 
        requires the output of the considered reaction and 
        the considered reaction is reversible:
        """
        if conting_state not in biological_states and \
            aux_state_1 not in biological_states and \
            aux_state_2 not in biological_states and \
            all_other not in biological_states and \
            cons_reaction not in reversible_reactions:
            pass

        else:
            print >> all_contingencies, \
            '''%s ; x %s (2) (old definition: %s state doesn't exist and %s is reversible)''' \
            % (cons_reaction, conting_state, conting_state, conting_state_reaction)

    # """If second order intersection exists:"""
    elif intersection_second_1 + '/' + aux_for_mark_1a in reaction_edges:

        if conting_state not in biological_states and \
            reaction_state not in biological_states:
            print >> all_contingencies, \
            '''%s ; x %s (3) (old definition: %s state doesn't exist and %s is reversible)''' \
            % (cons_reaction, conting_state, conting_state, conting_state_reaction)

    elif intersection_second_2 + '/' + aux_for_mark_1b in reaction_edges:

        if conting_state not in biological_states and \
            reaction_state not in biological_states:
            print >> all_contingencies, \
            '''%s ; x %s (4) (old definition: %s state doesn't exist and %s is reversible)''' \
            % (cons_reaction, conting_state, conting_state, conting_state_reaction)

    # """If third order intersection exists:"""
    elif 'abcd/' + aux_for_mark_2 in reaction_edges:

        if conting_state not in biological_states and \
            reaction_state not in biological_states:
            if aux_for_mark_1a not in biological_states:
                if aux_for_mark_1b not in biological_states:
                    print >> all_contingencies, \
                    '''%s ; x %s (5) (old definition: %s state doesn't exist and %s is reversible)''' \
                    % (cons_reaction, conting_state, conting_state, conting_state_reaction)


