#!/usr/bin/env python

"""
Apoptose model
Input for Species BID
"""

existing_reactions = ['A', 'B', 'C'] # All overlapping reactions for this species

reversible_reactions = ['B']

biological_states = ['a', 'ab', 'ac']   # Complexes: Conjunction of elemental states
reaction_edges = ['ab/a', 'ac/a']   # Reaction direction: ab/a means a -> ab

"""File in which contingencies for this species"""
all_contingencies = open('output_files/raw_contingencies_BID', 'w+')