#!/usr/bin/env python

"""
Contingencies Identification Script:

Identifies contingecies for 4 or less overlapping reactions. 
Reactions overlap if they have one species in common.

The required input is a network derived information about 
biological complexes and reaction edges. The input has to
be defined for each species separately and has to be written 
in a certain notation (see Network_based_Contingencies_Identification.pdf).
Hence, contingencies are found stepwise considering each species separately.
"""

from sys import argv

from for_4_reactions import find_contingencies_4reactions
from for_3_reactions import find_contingencies_3reactions
from for_2_reactions import find_contingencies_2reactions

"""
Input specification: 
"""
script_name, input_considered_species = argv

"""
Input import:
"""
exec("from " + input_considered_species + " import *")

number_of_reactions = len(existing_reactions) 
possible_elemental_states = map(str.lower, existing_reactions)


"""
Input for auxiliary variables definition: 
(Maximum number of reactions for this script is four, not scalable yet)
"""
all_reactions = ['A', 'B', 'C', 'D']
all_elemental_states = map(str.lower, all_reactions)

"""Dictionary for auxiliary variables"""
variable_dict = {}

"""File in which all variables get written"""
legend = open('output_files/Legend', 'w+')


for reaction in all_reactions:
    cons_reaction = reaction
    reaction_state = reaction.lower()

    """
    Define the considered contingency state, i.e. the modifier of the 
    considered reaction. 
    This is the state for which the contingency symbol will be identified 
    for the considered reaction defined previously. 
    """
    for state in all_elemental_states:
        if state != reaction_state:       
            conting_state = state
            conting_state_reaction = state.upper()
# """
# Define state that is a conjunction of all elemental states 
# except of the reaction state, e.g. this would be 'bcd' if 
# the reaction state is 'a'.
# """
            all_other_unsorted = []
            for state in all_elemental_states:
                if state != reaction_state:
                    all_other_unsorted.append(state)
            all_other_unsorted.sort()
            all_other = "".join([el for el in all_other_unsorted])


# """
# Define the (first order) intersection state of the reaction state and 
# the contingency state, e.g. 'ab' if 'a' is the reaction state and 'b' 
# the contingency state.
# """
            intersection_first_unsorted = [reaction_state, conting_state]
            intersection_first_unsorted.sort()
            intersection_first = "".join([el for el in intersection_first_unsorted])
            first = '1'

            
# """
# Define the two (second order) intersection states of the reaction state, 
# the contingency state and one of the other overlapping elemental states.
# Second order intersection states are for example 'abc' and 'abd' for 
# reaction state 'a' and contingency state 'b'.
# Further, definition of further auxiliary states (see Legend).
# """                 
            for i, not_considered_contingencies in enumerate(all_elemental_states):
                if not_considered_contingencies != reaction_state and not_considered_contingencies != conting_state:
                    not_conting_state = not_considered_contingencies                    
                    intersection_second_unsorted = [reaction_state, conting_state, not_conting_state]
                    intersection_second_unsorted.sort()
                    intersect_second = "".join([el for el in intersection_second_unsorted])

                    aux_state_unsorted = [conting_state, not_conting_state]
                    aux_state_unsorted.sort()
                    aux_state = "".join([el for el in aux_state_unsorted])
                    if first == '1':
                        other_1 = not_conting_state
                        intersection_second_1 = intersect_second
                        aux_state_1 = aux_state
                
                        first = '2'

                    else:
                        other_2 = not_conting_state
                        intersection_second_2 = intersect_second
                        aux_state_2 = aux_state                     
                        
                        first = '1'

                        variable_dict[cons_reaction + conting_state] = {'other_1': other_1, 
                                                  'intersection_second_1': intersection_second_1, 
                                                  'aux_state_1': aux_state_1,
                                                  'other_2': other_2, 
                                                  'intersection_second_2': intersection_second_2, 
                                                  'aux_state_2': aux_state_2}
                        
            aux_for_exclamation_mark_1a = [other_1, reaction_state]  # e.g.: for A; b -> ac
            aux_for_exclamation_mark_1b = [other_2, reaction_state]  # e.g.: for A, b -> ad
            aux_for_exclamation_mark_2 = [other_1, other_2, reaction_state] # e.g.: for A, b -> acd
            
            aux_for_exclamation_mark_1a.sort()
            aux_for_exclamation_mark_1b.sort()
            aux_for_exclamation_mark_2.sort()
            aux_for_mark_1a = "".join([el for el in aux_for_exclamation_mark_1a])   # ac
            aux_for_mark_1b = "".join([el for el in aux_for_exclamation_mark_1b])   # ad
            aux_for_mark_2 = "".join([el for el in aux_for_exclamation_mark_2])     # acd  

            
            print >> legend, 'Reaction for which contingencies are checked (considered reaction): ' + cons_reaction, '\n', \
                            'Elemental state of the considered reaction (reaction state): ' + reaction_state, '\n',\
                            'Elemental state which is the modifier of the considered reaction (contingency state): ' + conting_state, '\n', \
                            'First order intersection state (intersection_first): ' + intersection_first, '\n\n', \
                            '...and the combinatorial explosion (states required for the contingencies identification): ', '\n\n', \
                            'Not considered elemental state 1 (other_1): ' + variable_dict[cons_reaction + conting_state]['other_1'],'\n',\
                            'Second order intersection state 1 (intersection_second_1): ' + variable_dict[cons_reaction + conting_state]['intersection_second_1'], '\n',\
                            'Second order intersection state 1 \ reaction state (aux_state_1) : ' + variable_dict[cons_reaction + conting_state]['aux_state_1'], '\n',\
                            'Not considered state 2 (other_2): ' + variable_dict[cons_reaction + conting_state]['other_2'], '\n',\
                            'Second order intersection state 2 (intersection_second_2): ' + variable_dict[cons_reaction + conting_state]['intersection_second_2'], '\n',\
                            'Second order intersection state 2 \ reaction state (aux_state_2): ' + variable_dict[cons_reaction + conting_state]['aux_state_2'], '\n', \
                            'Auxiliary state 1a (aux_for_mark_1a): ' + aux_for_mark_1a, '\n', \
                            'Auxiliary state 1b (aux_for_mark_1b): '+ aux_for_mark_1b, '\n', \
                            'Auxiliary state 2 (aux_for_mark_2): ' + aux_for_mark_2, '\n', 'Conjunction of all states except of the reaction state: ' + all_other, '\n\n\n\n'
            

            """
            Checking contingencies for each reaction considering 
            each overlapping elemental state for this reaction separately
            """
            
            if cons_reaction in existing_reactions and conting_state in possible_elemental_states:
                
                if number_of_reactions == 4:
                    find_contingencies_4reactions(input_considered_species)
                    
                elif number_of_reactions == 3:
                    find_contingencies_3reactions(input_considered_species)

                elif number_of_reactions == 2:
                    find_contingencies_2reactions(input_considered_species)
