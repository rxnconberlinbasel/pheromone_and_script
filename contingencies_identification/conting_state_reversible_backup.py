#!/usr/bin/env python
"""
If contingency state is produced by a reversibel reaction:

Check wether the contingency is 'x' for the considered
reaction and the contingency state to avoid dissociaton. 

(Old definition for reversible reactions)
"""

def conting_state_rev(input_considered_species):
    """
    """

    """Input import:"""
    exec("from " + input_considered_species + " import *")

    """Variables import:"""
    from contingencies_identification_alpha import *

    if intersection_first + '/' + reaction_state in reaction_edges or \
        intersection_second_1 + '/' + aux_state_1 in reaction_edges or \
        intersection_second_2 + '/' + aux_state_2 in reaction_edges:
        
        if intersection_first +'/'+ reaction_state in reaction_edges and \
            intersection_first +'/'+ conting_state not in reaction_edges:

            # Check wether conting_state_reaction requires reaction_state and considered reaction is reversible:
            if conting_state not in biological_states and \
            aux_state_1 not in biological_states and \
            aux_state_2 not in biological_states and \
            all_other not in biological_states and \
            cons_reaction not in reversible_reactions:
                pass

            else:
                print >> all_contingencies, \
                '''%s ; x %s (2) (old definition: %s state doesn't exist and %s is reversible)''' \
                % (cons_reaction, conting_state, conting_state, conting_state_reaction)

        elif conting_state not in biological_states and \
            aux_state_1 not in biological_states and \
            aux_state_2 not in biological_states and \
            all_other not in biological_states:
            print >> all_contingencies,'%s ; x %s (3) (because %s is reversible)'\
            % (cons_reaction, conting_state, conting_state_reaction)

        
        elif intersection_first not in biological_states and \
            intersection_second_1 not in biological_states \
            and intersection_second_2 not in biological_states and \
            'abcd' not in biological_states:
            print >> all_contingencies, '%s ; x %s (4) (because %s is reversible)' \
            % (cons_reaction, conting_state, conting_state_reaction)
        
        elif aux_state_1 in biological_states and \
            intersection_second_1 + '/' + aux_state_1 not in reaction_edges or \
            aux_state_2 in biological_states and \
            intersection_second_2 + '/' + aux_state_2 not in reaction_edges or \
            all_other in biological_states and \
            'abcd/' + all_other not in reaction_edges and \
            intersection_second_1 not in biological_states and \
            intersection_second_2 not in biological_states:

            print >> all_contingencies, '%s ; x %s (5) (because %s is reversible)' \
            % (cons_reaction, conting_state, conting_state_reaction)


