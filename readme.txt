File structure and contents:

Folder "contingencies_identification": 
1. Contingencies_identification.py script 
2. Modules for contingencies_identification.py
3. Input files for 9 species of the apoptose model


Folder "contingencies_identification" -> "output_files":
Output files for the 9 species (identified contingencies in raw format)

Folder "contingencies_identification" -> "output_files" -> "Re-translated_and_complete":
1. Re-translated output files
2. complete_contingecies: all re-translated contingencies are merged into complete_contingencies


Folder "documentation_apoptose_model":
1. contingencies_identification_apoptose.pdf:
Documentation on the semi-automatic contingencies identification for the apoptose model
2. tex-files 
3. Paper including the rxncon apoptose model (M. Rother et al., Mol. BioSyst., 2013)


Folder "pheromone_model":
1. pheromone_rxncon_SBGN-PD.svg: Rxncon generated SBGN-PD graph
2. pheromone_rxncon_quick: Complete pheromone rxncon model in quick format
3. pheromone_ryncon.xls: Complete pheromone rxncon model in excel format

Folder "pheromone_model" -> "Kofahl_Klipp_model": 
1. Paper presenting the pheromone ODE model (Kofahl et al., Yeast, 2004)
2. BIOMD0000000032.xml: Curated SBML model
3. SBMLtab of the SBML model

Folder "pheromone_model" -> "Cytoscape": 
1. Cytoscape session of the SBML model
3. Cytoscape SBML file
3. Exported Cytoscape graph in pdf format

Folder "pheromone_model" -> "G_protein_cycle": 
1. Minimal sample model: 
The G protein cycle of the pheromone model in rxncon quick format
2. Summerizing overview of rxncon reactions and SBML reactions of the G protein cycle






